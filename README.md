# Welcome to the Boomi TCP Connector Open Source Project.

This connector provides a TCP client and server/listener for doing low level socket read/write operations.

To install the connector to your account, please follow the following instructions.

https://help.boomi.com/bundle/connectors/page/t-atm-Adding_a_connector_group.html

The connector-descriptor.xml and TCPConnector*.zip files are at the root of this project.


TODO refactor the listener with Netty http://tutorials.jenkov.com/netty/netty-tcp-server.html