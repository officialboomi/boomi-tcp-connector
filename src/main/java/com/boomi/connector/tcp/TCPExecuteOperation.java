package com.boomi.connector.tcp;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.util.BaseUpdateOperation;
import com.boomi.util.IOUtil;

public class TCPExecuteOperation extends BaseUpdateOperation {

	protected TCPExecuteOperation(TCPConnection conn) {
		super(conn);
	}

	@Override
	protected void executeUpdate(UpdateRequest request, OperationResponse response) {
		Logger logger = response.getLogger();
		PropertyMap connProps = getContext().getConnectionProperties();
		InputStreamReader isr = null;
		BufferedReader ibr = null;
		String host = connProps.getProperty("outboundHost");
		if (host == null || host.length() == 0)
			throw new ConnectorException("Host address must be specified");
		int port = connProps.getLongProperty("portNumber").intValue();
		Socket socket = null;

		for (ObjectData input : request) {
			InputStream docIs = input.getData();

			try {

				isr = new InputStreamReader(docIs);
				ibr = new BufferedReader(isr);
				socket = new Socket(host, port);
				// write to socket using ObjectOutputStream
				// oos = new ObjectOutputStream(socket.getOutputStream());
				logger.info("socket.isClosed()" + socket.isClosed());
				logger.info(String.format("Sending document to Socket Server: %s:%d",host,port));
	
				PrintWriter socketOut = null;
				
				//https://stackoverflow.com/questions/10113460/objectoutputstream-and-printwriter-conflict
				//The extra text at the beginning is coming from the object output stream.
				//Attaching an ObjectOutputStream AND a PrintStream to the same outputstream is basically just never going to work. 
				//You have to come up with a solution for using 1 or the other. To use just a PrintStream, you might consider 
				//converting your object(s) to JSON or XML. On the other hand, you could just use an ObjectOutputStream and write your strings to the ObjectOutputStream
//				socketOut = new PrintWriter(new ObjectOutputStream(socket.getOutputStream()), true);
				socketOut = new PrintWriter(socket.getOutputStream(), true);
				socketOut.write(readString(ibr));
				socketOut.flush();

				logger.info(String.format("Sending document to Socket Server: %s:%d", host, port));

				logger.info(String.format("Open socket input stream to get response"));
				BufferedReader socketIn = new BufferedReader(new InputStreamReader(socket.getInputStream(), StandardCharsets.UTF_8));
				StringBuilder socketResponse = new StringBuilder();
				int c;
				while ((c = socketIn.read()) != -1) {

					socketResponse.append((char) c);
					if (!socketIn.ready())
						break;
				}

				logger.info("clientSocket.isClosed()" + socket.isClosed());

				// create a payload from the socket response
				logger.info(String.format("Send input stream toPayload"));
				response.addResult(input, OperationStatus.SUCCESS, "OK", "Socket Write Successful",
						ResponseUtil.toPayload(new ByteArrayInputStream(socketResponse.toString().getBytes())));
				logger.info(String.format("Response from socket server"));

				// close resources
				socketOut.close();
				socket.close();

				try {
				} finally {
					IOUtil.closeQuietly(docIs);
				}

			} catch (Exception e) {
				// make best effort to process every input
				ResponseUtil.addExceptionFailure(response, input, e);
			}
		}
	}

	@Override
	public TCPConnection getConnection() {
		return (TCPConnection) super.getConnection();
	}

	private String readString(BufferedReader buffIn) throws IOException {
		StringBuilder everything = new StringBuilder();
		String line;
		while ((line = buffIn.readLine()) != null) {
			everything.append(line);
		}

		return everything.toString();
	}
}