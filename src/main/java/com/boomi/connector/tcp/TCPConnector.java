package com.boomi.connector.tcp;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.ConnectorContext;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.api.listen.ListenManager;
import com.boomi.connector.api.listen.ListenOperation;
import com.boomi.connector.util.listen.BaseListenConnector;
import com.boomi.connector.util.listen.UnmanagedListenConnector;
import com.boomi.connector.util.listen.UnmanagedListenOperation;

public class TCPConnector extends UnmanagedListenConnector{//BaseListenConnector {

    @Override
    public Browser createBrowser(BrowseContext context) {
        return new TCPBrowser(createConnection(context));
    }    

    @Override
    protected Operation createExecuteOperation(OperationContext context) {
        return new TCPExecuteOperation(createConnection(context));
    }
   
    private TCPConnection createConnection(BrowseContext context) {
        return new TCPConnection(context);
    }

//	@Override
//	public ListenManager createListenManager(ConnectorContext context) {
//		return new TCPListenManager(context);
//	}

//	@Override
//	public MananagedListenOperation createListenOperation(OperationContext context) {
//		return new TCPUnmanagedListenOperation(context);
//	}
	
	@Override
	public UnmanagedListenOperation createListenOperation(OperationContext context) {
		return new TCPUnmanagedListenOperation(context);
	}
}