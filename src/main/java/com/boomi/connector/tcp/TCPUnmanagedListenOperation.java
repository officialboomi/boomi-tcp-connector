package com.boomi.connector.tcp;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.logging.Logger;
import java.util.stream.Stream;
import java.util.concurrent.Executors;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.api.PayloadUtil;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.listen.Listener;
import com.boomi.connector.api.listen.ListenerExecutionResult;
import com.boomi.connector.api.listen.SubmitOptions;
import com.boomi.connector.api.listen.options.WaitMode;
import com.boomi.connector.util.listen.UnmanagedListenOperation;
import com.boomi.util.LogUtil;

public class TCPUnmanagedListenOperation extends UnmanagedListenOperation {

    private static final Logger logger = LogUtil.getLogger(TCPUnmanagedListenOperation.class);
    ServerSocket _socketServer;
    Listener _listener;
    String _operationMode;
    SocketThread _socketThread;
    SocketServerThread serv;
    PropertyMap connProps;


    /**
     * Creates a new instance using the provided operation context
     */
    protected TCPUnmanagedListenOperation(OperationContext context) {
        super(context);
        logger.info("Constructing TCPUnmanagedListenOperation");
    }

    @Override
    protected void start(Listener listener) {
        _listener = listener;
         connProps = getContext().getConnectionProperties();
        _operationMode = getContext().getOperationProperties().getProperty("operationMode");
        int port = connProps.getLongProperty("portNumber").intValue();
        try {
            _socketServer = new ServerSocket(port);
            serv = new SocketServerThread(_socketServer,_operationMode);
            Thread t = new Thread(serv);
            t.start();

        } catch (IOException e) {
            throw new ConnectorException(e);
        }
    }

    /**
     * Stops the listen operation by deregistering with the manager
     */
    @Override
    public void stop() {
        logger.info("TCPUnmanagedListenOperation.stop");
        if (_socketServer != null)
            try {

                _socketServer.close();
                _socketServer = null;
                serv.doStop();
                this._listener = null;
            } catch (IOException e) {
                throw new ConnectorException(e);
            }

    }

    public class SocketServerThread implements Runnable {

        private Socket clientSocket;
        private String _operationMode;
        private ServerSocket _socketServer;
        private ExecutorService _pool;

        public SocketServerThread(ServerSocket server,String opsMode) {
            _socketServer = server;
            _operationMode=opsMode;
            int maxPThreadPoolSize = connProps.getLongProperty("maximumConnections").intValue();
            _pool = Executors.newFixedThreadPool(maxPThreadPoolSize);
            logger.info("TCPUnmanagedListenOperation.started with Thread Pool Size: " + maxPThreadPoolSize);
        }


        private boolean doStop = false;

        public synchronized void doStop() {

            _pool.shutdown();
             this.doStop = true;
        }





        @Override
        public void run() {


            try {
                while (!this.doStop) {
                    clientSocket = this._socketServer.accept();
                   _socketThread = new SocketThread(clientSocket,_operationMode);
                    _pool.execute(_socketThread);
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }


        }
    }


    public class SocketThread implements Runnable {

        private final Socket clientSocket;
        private String _operationMode;
        private ServerSocket _socketServer;


        public SocketThread(Socket clientSocket,String opMode) {

            this.clientSocket = clientSocket;
            _operationMode = opMode;

        }


        private boolean doStop = false;

        public synchronized void doStop() {
            this.doStop = true;
            _socketServer = null;
        }

        private synchronized boolean keepRunning() {
            return this.doStop == false;
        }


        @Override
        public void run() {


            logger.info("TCPUnmanagedListenOperation.run");

            //TODO We assume the socket was closed due to a listener stop
            if (clientSocket == null || clientSocket.isClosed()) {
                logger.info("client socket is null or closed, likely do to the listener being stopped. ");
                return;
            }
            try {
                logger.info("open socket input stream to read the request");
                /* Wrap Inputstream over a reader else socket is closing */
                BufferedReader socketIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                int c;
                StringBuffer socketResponse = new StringBuffer();

                while ((c = socketIn.read()) != -1) {

                    socketResponse.append((char) c);
                    if (!socketIn.ready())
                        break;

                }

                StringBuilder response = new StringBuilder();
                if (_operationMode != null && _operationMode.contentEquals("sync")) {
                    logger.info("operationMode sync and wait for future");

                    SubmitOptions submitOptions = new SubmitOptions().withWaitMode(WaitMode.PROCESS_COMPLETION);
                    Future<ListenerExecutionResult> future = _listener.submit(
                            PayloadUtil.toPayload(new ByteArrayInputStream(socketResponse.toString().getBytes())),submitOptions );

                    while (!future.isDone()) {

                    }

                    logger.info("TCPUnmanagedListenOperation.run After !future.isDone()");
                    logger.info("Execution ID: " + future.get().getExecutionId() + " SUCCESS: " + future.get().isSuccess());
                    if (future.get().isSuccess()) {
                        //Read the file work/<executionId>.response and write that back to the client
                        String responseFile = "work/" + future.get().getExecutionId() + ".response";

                        try (Stream<String> stream = Files.lines(Paths.get(responseFile), StandardCharsets.UTF_8)) {
                            stream.forEach(s -> response.append(s).append("\n"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        File fileToDelete = new File(responseFile);
                        fileToDelete.delete();

                    } else {
                        response.append("ERROR");
                    }
                } else {
                    logger.info("operationMode async");
                    _listener.submit(PayloadUtil.toPayload(new ByteArrayInputStream(socketResponse.toString().getBytes())));
                    response.append("OK");
                }
                // TODO write response to socket
                logger.info("open socket output stream to write the response");
                /* Wrapped around a PrintWriter */
                PrintWriter socketOut = new PrintWriter(clientSocket.getOutputStream(), true);
                // out.write(readString(ibr));

                // OutputStream os = clientSocket.getOutputStream();
                logger.info("Writing response to socket");
                socketOut.write(new String(response.toString().getBytes()));
                socketOut.flush();
            } catch (IOException | InterruptedException | ExecutionException e) {
                throw new ConnectorException(e);
            }
            /*}*/
        }
    }
}