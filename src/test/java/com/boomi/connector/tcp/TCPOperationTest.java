// Copyright (c) 2013 Boomi, Inc.

package com.boomi.connector.tcp;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.*;

import com.boomi.connector.api.OperationContext;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.api.listen.Listener;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleOperationResult;

/**
 * @author Dave Hock
 *
 * 01/09/2020(Prem): Refactored to work with the Threadpool by ordering the method exection
 */

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TCPOperationTest 
{


	static TCPUnmanagedListenOperation _operation ;
    static final long _portNum = 9907;

    @Test
    @Order(1)
    public void testStartListener()
    {


   //new Thread(new TestListener(9907, "async")).start();
    new Thread(new TestListener(9908, "sync")).start();

    }
	
    @Test
    @Order(2)
    public void testExecuteOperationAsync() throws Exception
    {
        int howManyThreads=10;

        for (int i=0;i<howManyThreads;i++) {

            new TestTCPClient("sync",9908).run();



        }

    }
    
   @Test
    @Order(3)
    public void testExecuteOperationSync() throws Exception
    {

        int howManyThreads=10;

        for (int i=0;i<howManyThreads;i++) {
            new TestTCPClient("sync",9908).run();

        }

    }


}


class TestListener implements Runnable {

    static Listener _listener ;
    private long _portNum = 9907;
    private String _mode;
    static final long _threadPoolSize=1;
    static TCPUnmanagedListenOperation _operation ;
     public TestListener(long port,String opsMode)
     {
         _portNum=  port;
         _mode=opsMode;


     }

    @Override
    public void run() {

        _listener = new MockListener();

        TCPConnector connector = new TCPConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put("portNumber", _portNum);
        connProps.put("maximumConnections", _threadPoolSize);
        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put("operationMode", _mode);
        tester.setOperationContext(OperationType.LISTEN, connProps, opProps, null, null);
        _operation = new TCPUnmanagedListenOperation(tester.getOperationContext());
        _operation.start(_listener);
    }
    }


class TestTCPClient extends Thread {


   private long _portNum = 9907;
    private String opMode;
    public TestTCPClient(String mode, long port)
    {
        opMode=mode;
        _portNum=port;
   }

    @Override
    public void run() {



        long start=System.currentTimeMillis();
        TCPConnector connector = new TCPConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put("outboundHost", "localhost");
        connProps.put("portNumber", _portNum);

        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put("operationMode", opMode);
        tester.setOperationContext(OperationType.EXECUTE, connProps, opProps, null, null);
        List<InputStream> inputs = new ArrayList<InputStream>();
        inputs.add(new ByteArrayInputStream("HELLO from JUNIT".getBytes()));
        List<SimpleOperationResult> actual = tester.executeExecuteOperation(inputs);

        assertEquals("OK", actual.get(0).getStatusCode());

        System.out.println("Total Time"+(System.currentTimeMillis()-start));
      //  assertEquals("Hello sync response from your listener. Here is your input echoed back: HELLO from JUNIT\n", new String(actual.get(0).getPayloads().get(0)));


    }
}